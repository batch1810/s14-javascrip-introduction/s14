// console.log("Hello World!");

// // [SECTION] Syntax, Statements, and Comments

// // Statements in programming are instructions that we tell the computer to perform.
// 	// Semicolon to end a statement. (JS doesn't require semicolons)

// // Syntax in programming, it is the set of rules that how statements must be constructed.

// // Comments are meant to describe the written code.

// /*

// 	There are two types of comments:
// 	1. Single line comments denoted by two slashes Windows:(ctrl + /) Mac:(cmd + /)
// 	2. Multi-line comments denoted by a slash and asterisk Windows:(ctrl + shift + /) Mac:(cmd + option + /)

// */

// // [SECTION] Variables

// // It is used to contain data.

// // Declaring a Variable
// // It tells our devices that a variable name is created and is ready to store data
// /*
// 	let/const variableName;

// */
	
// let myVariable;
// // is used to print values of certain variables or result into the Browser's Console.
// console.log(myVariable);
// // undefined means the variable's value was not defined.

// // Resulted with error "not defined"
// // Variable must be declared first before they are used.
// // Equal sign (=) is used to assign value in a variable.
// let hello = "Hi";
// console.log(hello);

// /*
// 	Guides in writing variables:
// 	1. Use the "let" keyword followed by the variable name of your choice and use assignment operator (=) to assign a value.
// 	2. Variable names names should start with a lowecase character, use camelCase for multiple words.
// 		one word: variable
// 		multiple words: myVariable
// 	3. For constant variables, use the "const" keyword.
// 	4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.

// */

// 	/*
// 		var vs let/const

// 		-let and const
// 			- It cannot be re-declared into the scope.

// 		-var ES1 (1997)
// 			- It can be re-declared into the scope.

// 	*/

// // Declaring and initializing
// // This is the instance when a variable is given its initial/starting value.
// /*
// 	Syntax:
// 		let/const variableName = value;

// */

// let productName = "Desktop Computer";
// console.log(productName);

// let productPrice = 18999;
// console.log(productPrice);

// const interest = 3.539;
// console.log(interest);

// // Reassigning variable values
// // changing the initial or previous value into another value
// /*
// 	Syntax:
// 		variableName = newValue;

// */

// productName = "Laptop";
// console.log(productName);

// // Will return an error because reassgining a value with a constant variable
// // interest = 4.489; 
// // console.log(interest);

// // Reassigning vs Initializing variables

// let supplier;

// // Initializing variable
// 	// assigning the initial value.
// supplier = "John Smith Tradings";
// console.log(supplier);

// // Reassigning variable
// 	// change the initial value of the variable.
// supplier = "Zuitt Store";
// console.log(supplier);

// // Multiple variable declaration and initialization

// const productCode = "DC017", productBrand = "Dell";
// console.log(productCode, productBrand);

// // Reserved keywords as variable
// // Using a reserved keyword will cause an error.
// // const let = "hello";

// // console.log(let);

// // [SECTION] Data Types

// // String
// // String are a series of characters that create a word, a phrase, a sentence or anythiong related to creating a text.
// // Javascript Strings can be written using either Double Quote ("") or Single Quote ('').

// // For other programming language 'a'- single characters while "Hello" - series of characters.

// let country = 'Philippines';
// let province = "Metro Manila";

// console.log(country, province);

// // I live in the Metro Manila, Philippines

// // Concatenating Strings
// // Multiple string values can be combined to create a single string using the "+" symbol
// let fullAddress = 'I live in the ' + province + ', ' + country;
// console.log(fullAddress);

// /*
// 	Mini Activity Instructions:
// 		1. Create variables that will contain your firstName and lastName.
// 		2. Reassign the value of the province variable with the province you live in.
// 		3. Reassign the value of the fullAddress variable with this sentence:
// 			Hello, I am firstName lastName. I live in province, country.
// 			Example:
// 				Hello, I am Angelito Quiambao. I live in Tarlac, Philippines.
// 		4. Print the new value of the greeting variable using console log.
// 		5. Once done, take a screenshot of your browser console and send it to the batch hangouts.
// */

// let firstName = "Angelito", lastName = "Quiambao";
// province = "Tarlac";

// fullAddress = "Hello, I am " + firstName + " " + lastName + ". I live in " + province + ", " + country + ".";
// console.log(fullAddress);

// // Escape characters (\) in combination with string characters to produce different result.
// // \n creating a new line between text
// let mailAddress = "Metro Manila\nPhilippines";
// console.log(mailAddress);

// let message = "John's employees went home early.";
// console.log(message);

// message = 'John\'s employees went home early.';
// console.log(message);

// //Number
// // Interger/Whole Number
// let headcount = 26;
// console.log(headcount);

// // Decimal Number/Fractions
// let grade = 98.7;
// console.log(grade);

// // Exponential Notation
// let planetDistance = 2e10;
// console.log(planetDistance);

// // Combining number and string
// console.log("John's grade last quarter is " + grade);

// // Boolean
// // Boolean values are normally used to store values relating to the state of certain things.
// let isMarried = false;
// let isGoodConduct = true;

// console.log("isMarried: " + isMarried);
// console.log("isGoodConduct: " + isGoodConduct);

// // Arrays
// // Arrays are a special kind of data type that's used to store multiple values with the same data type.

// // let grade1 = 98.7,  grade2 = 92.1, grade3 = 90.5;
// // console.log(grade1, grade2, grade3);
// /*
// 	Syntax:
// 	let/const arrayName = [elementA, elementB, elementC];

// */
// 			// 0 	1 		2  	3
// let grades = [98.7, 92.1, 90.2, 94.6];
// console.log(grades);
// // Array index is a number identifying the place of elements.
// // Array elements usually starts with 0 to n.
// // to identify the last element of the n-1

// console.log(grades[3]);

// // different data types
// 	// it is not recommended
// let details = ["John", "Smith", 32, true];
// console.log(details);

// // Object
// // Object are another special kind of data type that's used to mimic real world objects/items.

// /*
// 	Syntax:

// 		let/const objectName = {
// 			propertyA: value,
// 			propertyB: value
// 		}
// */

// let person = {
// 	fullName: "Juan Dela Cruz",
// 	age: 35,
// 	isMarried: false,
// 	contactNo: ["+63917 123 56676", "8123 4567"],
// 	address: {
// 		houseNumber: "345",
// 		city: "Manila"
// 	}
// }

// console.log(person);

// // Select a property of an object
// 	// dot notation is used to access a specific property of an object

// console.log(person.fullName);
// console.log(person.isMarried);
// // object property within an object
// console.log(person.address.city);
// console.log(person.contactNo[0]);

// // Abstract Objects
// let myGrades = {
// 	Mathematics: 98.7,
// 	Science: 92.1,
// 	History: 90.2,
// 	English: 94.6
// }

// console.log(myGrades);

// // Null and Undefined
// // Null is an assigned value but it means nothing.
// // Undefined is a value result from a variable declared without initialization.

// let spouse = null;
// console.log(spouse);

// let name;
// console.log(name);


//Fuction

function printName(name){
	console.log(name);
	console.log("My name is " + name);
};

//Invoking/Calling Functions
printName("Jane");
printName("John");

console.log(name);

function createFullName(fName,lName,mName){
	console.log(fName + ' '+mName + ' ' +lName);
};
createFullName("Juan", "Dela", "Cruz");

createFullName("Juan", "DelaCruz" );
createFullName("Juan", "Dela", "Cruz","Hello");

let fName = "Pres";
let mName = "Pres";
let lName = "Pres";

// variable as parameter
createFullName(fName,mName,lName);

//return statement
//it allows the output of a function to be passed to a block of code

function returnFullName(fName,mName,lName){
	return fName+mName+lName;
	console.log("completeName");

};

let completeName = returnFullName(fName,mName,lName);
console.log(completeName);
