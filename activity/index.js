console.log("Hello World!");
// 3. Create multiple variables that will store the different JavaScript data types containing information relating to user details
let firstName = "John", lastName = "Smith";
console.log("First Name: "+firstName);
console.log("Last Name: "+lastName);
let age = 30;
console.log("Age: "+age);
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
console.log("Hobbies:");
console.log(hobbies);
let workAddress = {
	houseNumber: "32",
	street: "Washington",
	City: "Lincoln",
	State: "Nebraska",
	};
console.log("Work Address:");
console.log(workAddress);
let isMarried = true;


/*// 4. Create a function named printUserInfo that will accept the following information:
- First name
- Last name
- Age
5. The printUserInfo function will print those details in the console as a single string. 
6. This function (printUserInfo) will also print the hobbies and work address.*/

function printUserInfo(firstName,lastName,age){
	console.log(firstName+" "+ lastName+ " " + "is"+ " "+ age + " " + "years of age");
	console.log("This was printed inside of a fuction");
	console.log(hobbies);
	console.log("This was printed inside of a fuction");
	console.log(workAddress);
};

printUserInfo(firstName,lastName,age);

// 7. Create another function named returnFunction that will return a value and store it in a variable.
function returnFunction(isMarried){
	return "The value of isMarried is: "+isMarried;
};

console.log(returnFunction(isMarried));